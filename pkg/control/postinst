#!/bin/bash

################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
#
# Final setup. Note that this doesn't automatically execute
# my_ros_env.sh. The next time bash starts
# that script should run and set up ROS environment variables.
#
# author: alex.gardner@modalai.com
################################################################################

set -e

## configure some android properties
setprop video.disable.ubwc 1
setprop persist.camera.preview.ubwc 0
setprop persist.camera.analysis.enable 0
setprop persist.stereo.analysis.enable 0

# Set default ssh shell to bash
chsh -s /bin/bash root

if [ -d "/opt/ros/indigo/share/compressed_image_transport/" ] ; then
    #echo "Removing compressed_image_transport ros plugin"
    rm -rf "/opt/ros/indigo/share/compressed_image_transport"
fi
if [ -d "/opt/ros/indigo/share/compressed_depth_image_transport" ] ; then
    #echo "Removing compressed_depth_image_transport ros plugin"
    rm -rf "/opt/ros/indigo/share/compressed_depth_image_transport"
fi

# stop the QC init_post_boot script from messing with our FS settings
sed -i "s/ echo 200 > \/proc\/sys\/vm\/dirty_expire_centisecs/ #echo 200 > \/proc\/sys\/vm\/dirty_expire_centisecs/" /etc/initscripts/init_post_boot

# install the ros script, no-clobber to avoid overwriting user changes
# also set executable bit with chmod since 'install' doesn't have no-clobber
#
# make sure ros-env exists and is the correct version
ROS_VERSION=$( cat /usr/share/modalai/system-tweaks/template_ros_env.sh | grep "VERSION" )
if [ ! -f /home/root/.profile.d/ros_env.sh ] ||
    ! cat /home/root/.profile.d/ros_env.sh | grep -q "$ROS_VERSION" ; then

    echo "making /home/root/.profile.d/"
    mkdir -p /home/root/.profile.d/
    echo "copying ros env template"
    cp /usr/share/modalai/system-tweaks/template_ros_env.sh /home/root/.profile.d/ros_env.sh
    chmod +x /home/root/.profile.d/ros_env.sh
fi

if [ -f /home/root/my_ros_env.sh ]; then
    echo "removing old symlink /home/root/my_ros_env.sh"
    rm -f /home/root/my_ros_env.sh
fi

echo "making new symlink /home/root/my_ros_env.sh"
ln -s /home/root/.profile.d/ros_env.sh /home/root/my_ros_env.sh

# copy our tweaked wifi initscript over the old one
WIFISCIPT="/etc/initscripts/wlan"
cp -f /usr/share/modalai/system-tweaks/modal_wlan_initscript $WIFISCIPT

