#!/bin/bash

################################################################################
# script to set up user-specific ros environment variables. This should be kept
# as /home/root/ros_env.sh on the robot and should be called by .bashrc
# so that it is loaded when bash starts.
#
# author: james@modalai.com
################################################################################

#VERSION 1.0

# load main ros environment
source /opt/ros/indigo/ros-env.sh

# if a catkin workspace is setup then make sure the launch
# files and run files are available in the ROS PATH
if [ -f /home/root/catkin_ws/devel/setup.bash ]; then
    source /home/root/catkin_ws/devel/setup.bash
fi

if [ -f /home/root/catkin_ws/install/setup.bash ]; then
    source /home/root/catkin_ws/install/setup.bash
fi


# Set ROS_IP & ROS_MASTER_URI appropriately for your configuration
# 192.168.8.1 is default for the robot in soft access point mode
export ROS_IP=192.168.8.1
export ROS_MASTER_IP=${ROS_IP}
export ROS_MASTER_URI=http://${ROS_IP}:11311/
unset ROS_HOSTNAME
